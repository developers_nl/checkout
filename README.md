checkout
=========

This role is primarily intended to create a git-sync sidecar docker container but can be usefull in other cases
If this role is used to build a docker container one can optionally supply all the variables at build time 
or as environment vars on runtime using the below variables or a combination of both.

    GIT_CHECKOUT_USER
    GIT_CHECKOUT_GROUP
    GIT_CHECKOUT_REPO
    GIT_CHECKOUT_BRANCH
    GIT_CHECKOUT_DEST
    GIT_CHECKOUT_REV
    GIT_CHECKOUT_USERNAME_FILE
    GIT_CHECKOUT_PASSWORD_FILE
    GIT_CHECKOUT_EXPECT_TIMEOUT     


Requirements
------------

This role uses the dig lookup so dig (bind-tools in alpine) must be installed on the host running the playbook

Role Variables
--------------
required variables:

    git_repo_uri: # can be a https or ssl uri 

If it is a ssl url a git_private_key(_file) needs to be provided with an optional git_password(_file). 
If https is used, optionally a git_username(_file) and git_password(_file) can be provided.
The if both are provided (eg git_username and git_username_file) this role will try to write the value to the file
If only a value (eg git_username) is provided is is written to a file

    git_private_key: |
      -----BEGIN RSA PRIVATE KEY-----
      Proc-Type: 4,ENCRYPTED
      DEK-Info: DES-EDE3-CBC,3E4F6ECC21F721DB
    
      ..............
    
      -----END RSA PRIVATE KEY-----
    git_private_key_file: "{{ git_secret_base_dir }}/private_key"
    git_username: myusername
    git_username_file: "{{ git_secret_base_dir }}/username"
    git_password: My_SuPer_PaSsWoRd
    git_password_file: "{{ git_secret_base_dir }}/password"  

Available variables are listed below, along with default values (see defaults/main.yml):
    
    git_checkout_dest_owner: nobody
    git_checkout_dest_group: nobody
    git_checkout_dest: /data
    git_checkout_branch: master 
    git_uri_match_regex: '^(https(://)|git)(([^\s]*?)@)?([^\s]*?)(:|/)([^\s]*)$' #regex matching git uris, used for known_hosts file
    git_checkout_expect_timeout: 120
    git_secret_base_dir:  /run/secrets/git
    
Below values are optional
    
    git_checkout_rev: d23d172249ddb23cbeb49e0384aa6ad6ad97940c # git hash
     

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

A minimal example of a play:

    - hosts: all
      vars:
        git_repo_uri: https://bitbucket.org/developers_nl/checkout.git
      roles:
         - checkout

A more advanced version:
    
    - hosts: all
      vars:
        git_repo_uri: git@bitbucket.org:developers_nl/checkout.git
        git_private_key: |
          -----BEGIN RSA PRIVATE KEY-----
          Proc-Type: 4,ENCRYPTED
          DEK-Info: DES-EDE3-CBC,3E4F6ECC21F721DB
        
          ..............
        
          -----END RSA PRIVATE KEY-----
        git_checkout_branch: develop
        git_checkout_rev: d23d172249ddb23cbeb49e0384aa6ad6ad97940c
      roles:
         - checkout

License
-------

BSD